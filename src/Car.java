public class Car {
	Integer carId;
	String name = "";
	double price = 0.0;
	double resaleValue = 0.0;
	String model;

	Car(Integer carId, String name, double price) {
		this.carId = carId;
		this.name = name;
		this.price = price;
		this.resaleValue = this.price * .60;

	}

	public String toString() {
		return "Car ID:" + carId + "\t" + "Name:" + name + "\t" + "Model:"
				+ model + "\t" + "price:" + price + "\t" + "resalevalue:"
				+ resaleValue;
	}

}
