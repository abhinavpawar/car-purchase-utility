import java.util.*;
import java.io.*;
import java.util.Collections;

public class Controller implements AppConstants {

	static ArrayList<Customer> customersList = new ArrayList<Customer>();
	static BufferedReader br = new BufferedReader(new InputStreamReader(
			System.in));
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws Exception {
		Integer userChoice;

		Boolean loopMain = true;
		do {

			System.out.println("Enter your preference:\n");
			System.out.println("1.Add new customer\n");
			System.out.println("2.Add new car\n");
			System.out.println("3.List all customers by name\n");
			System.out.println("4.Find customer by ID\n");
			System.out.println("5.Generate prize\n");
			System.out.println("6.Exit\n");

			userChoice = scanner.nextInt();

			switch (userChoice) {

			case ADD_CUSTOMER:
				addNewCustomer();
				break;
			case ADD_CAR:

				addNewCar();
				break;
			case LIST_ALL:
				listAllCustomersByName();
				break;
			case FIND_CUSTOMER:
				findCustomerById();
				break;
			case PRIZE:
				prize();
				break;
			case EXIT:
				loopMain = false;
				break;
			default:
				System.out.println("Invalid Choice !");
				break;
			}

		} while (loopMain);

	}

	static void addNewCustomer() throws IOException {

		System.out.println("Enter customer details:\n");
		System.out.println("Enter customer ID \n");
		int custId = scanner.nextInt();
		System.out.println("Enter customer name\n");
		String custName = br.readLine();

		Customer customer = new Customer(custId, custName);

		customersList.add(customer);
	}

	static void findCustomerById() {
		Integer custId;

		System.out.println("Enter Customer ID\n");
		custId = scanner.nextInt();
		for (Customer customer : customersList) {
			if (custId == customer.getId())
				customer.display();
		}
	}

	static void addNewCar() throws IOException {
		Integer custId;
		String replyNewCar;
		Boolean flag = false;
		System.out.println("Enter Customer ID\n");
		custId = scanner.nextInt();
		Customer tempCustomer = new Customer();
		for (Customer tempObject : customersList) {
			if (custId == tempObject.getId()) {
				flag = true;
				tempCustomer = tempObject;

			}
		}
		if (flag == true) {
			do {
				System.out.println(" Enter car ID\n");
				int carId = scanner.nextInt();
				System.out.println("Enter car model \n");
				System.out.println("1.Hyundai \n");
				System.out.println("2.Maruti \n");
				System.out.println("3.Toyota \n");

				Integer carModel = scanner.nextInt();
				System.out.println("Enter car name \n");
				String carName = br.readLine();
				System.out.println("Enter price \n");
				double carPrice = scanner.nextDouble();

				switch (carModel) {
				case HYUNDAI:
					Hyundai tempObjectHyundai = new Hyundai(carId, carName,
							carPrice);
					tempCustomer.carList.add(tempObjectHyundai);
					break;
				case MARUTI:
					Maruti tempObjectMaruti = new Maruti(carId, carName,carPrice);
					tempCustomer.carList.add(tempObjectMaruti);
					break;
				case TOYOTA:
					Toyota tempObjectToyota = new Toyota(carId, carName,
							carPrice);
					tempCustomer.carList.add(tempObjectToyota);
					break;
				default:
					System.out.println("Invalid Model");
					break;
				}

				System.out.println("Do you want to add another car?\n");
				replyNewCar = br.readLine();

			} while (replyNewCar.equals("Y") || replyNewCar.equals("y"));

		} else if (flag == false) {
			System.out.println("Customer not found !");
		}

	}

	private static Comparator<Customer> byName() {
		return new Comparator<Customer>() {
			public int compare(Customer o1, Customer o2) {
				return o1.name.compareTo(o2.name);
			}
		};
	}

	public static void listAllCustomersByName() {
		Collections.sort(customersList, byName());
		for (int i = 0; i < customersList.size(); i++) {
			Customer tempCustomer = customersList.get(i);
			// System.out.println(customer);
			tempCustomer.display();

		}
	}

	static void prize() {
		int listSize = 0, i = 0;
		int adminChoice[] = new int[3];

		System.out.println("Enter your 3 preferences(CustomerID):\n");
		while (i < 3) {
			adminChoice[i] = scanner.nextInt();
			i++;
		}

		Set<Integer> selectedListFinal = new HashSet<Integer>();
		Random random = new Random();
		Set<Integer> selectedList = new HashSet<Integer>();

		while (listSize < 6) {
			Customer tempObject = new Customer(customersList.get(random
					.nextInt(customersList.size())));
			selectedList.add(tempObject.getId());
			listSize = selectedList.size();
		}
		// size = selectedList.size();
		for (i = 0; i < 3; i++) {
			if (selectedList.contains(adminChoice[i])) {
				selectedListFinal.add(adminChoice[i]);
			}
		}

		// System.out.println(selectedList);
		System.out.println(selectedListFinal);
	}

}
