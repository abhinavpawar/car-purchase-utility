public class Maruti extends Car {

	Maruti(Integer carId, String name, double price) {
		super(carId, name, price);
		resaleValue = price * .60;
		model = "maruti";
	}

	/*
	 * @Override public String toString() { return super.toString(); }
	 */

}
