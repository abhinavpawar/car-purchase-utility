import java.util.*;

public class Customer {
	Integer custId;
	String name;
	ArrayList<Car> carList = new ArrayList<Car>();

	Customer() {

	}

	Customer(Integer custID, String name) {
		this.custId = custID;
		this.name = name;

	}

	Customer(Customer C) {
		this.custId = C.custId;
		this.name = C.name;

	}

	Integer getId() {
		return (custId);
	}

	void display() {
		System.out.println("Customer id:" + this.custId);
		System.out.print("Customer name " + this.name);
		System.out.println("\n" + carList);

	}
}
