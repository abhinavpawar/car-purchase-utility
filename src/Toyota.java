public class Toyota extends Car {

	Toyota(Integer carId, String name, double price) {
		super(carId, name, price);
		resaleValue = price * .80;
		model = "toyota";

	}

	/*
	 * public String toString() { return super.toString(); }
	 */
}
