public class Hyundai extends Car {

	Hyundai(Integer carId, String name, double price) {

		super(carId, name, price);
		resaleValue = price * .40;
		model = "hyundai";

	}

}
